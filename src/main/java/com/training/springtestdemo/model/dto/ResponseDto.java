package com.training.springtestdemo.model.dto;

import com.training.springtestdemo.model.Item;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResponseDto {
    private boolean isSuccess;
    private Item item;
}
