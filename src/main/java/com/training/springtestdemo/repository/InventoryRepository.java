package com.training.springtestdemo.repository;

import com.training.springtestdemo.model.Item;
import com.training.springtestdemo.model.dto.ResponseDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class InventoryRepository {
    private static final Map<Integer, Item> itemRepository = new HashMap<>();

    public boolean addItem(Item item) {
        try {
            itemRepository.put(item.getCode(), item);
            log.info("Adding item with code {} successful", item.getCode());
        } catch (Exception e) {
            log.error("Exception while adding item: " + e);
            return false;
        }
        return true;
    }

    public ResponseDto getItem(int code) {
        try {
            log.info("Getting item with code {}", code);
            Item item = itemRepository.get(code);
            return item == null ? new ResponseDto(false, null) : new ResponseDto(true, item);
        } catch (Exception e) {
            log.error("Exception while getting item: " + e);
            return new ResponseDto(false, null);
        }
    }

    public boolean updateItem(@RequestBody Item item) {
        try {
            itemRepository.put(item.getCode(), item);
            log.info("Updating item with code {} successful", item.getCode());
        } catch (Exception e) {
            log.error("Exception while updating item: " + e);
            return false;
        }
        return true;
    }

    public boolean deleteItem(int code) {
        try {
            itemRepository.remove(code);
            log.info("Removing item with code {} successful", code);
        } catch (Exception e) {
            log.error("Exception while removing item: " + e);
            return false;
        }
        return true;
    }

}
