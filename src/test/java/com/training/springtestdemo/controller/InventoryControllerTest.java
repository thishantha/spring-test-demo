package com.training.springtestdemo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.training.springtestdemo.model.Item;
import com.training.springtestdemo.service.InventoryService;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class InventoryControllerTest {

    ObjectMapper mapper;
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private InventoryService inventoryService;

    @Before
    public void setup() {
        mapper = new ObjectMapper();
    }


    /**
     * Test deleting of item with mocking of the response from service
     */
    @Test
    @WithMockUser(username = "user", password = "user", roles = "USER")
    public void addItemWithUserTest() {

        try {
            Item item = new Item("test1", 1, 100);
            String json = mapper.writeValueAsString(item);
            this.mockMvc.perform(post("/api/inventory").content(json).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    public void deleteItemWithAdminTest() {
        Item item = new Item("test1", 1, 100);
        String json = null;
        try {
            json = mapper.writeValueAsString(item);
            this.mockMvc.perform(post("/api/inventory").content(json).contentType(MediaType.APPLICATION_JSON));
            this.mockMvc.perform(delete("/api/inventory/code=1")).andExpect(status().isOk());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
