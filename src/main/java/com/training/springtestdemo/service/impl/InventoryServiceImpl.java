package com.training.springtestdemo.service.impl;

import com.training.springtestdemo.model.Item;
import com.training.springtestdemo.model.dto.ResponseDto;
import com.training.springtestdemo.repository.InventoryRepository;
import com.training.springtestdemo.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InventoryServiceImpl implements InventoryService {
    @Autowired
    InventoryRepository inventoryRepository;

    @Override
    public ResponseDto addItem(Item item) {
        return new ResponseDto(inventoryRepository.addItem(item), null);
    }

    @Override
    public ResponseDto getItem(int code) {
        return inventoryRepository.getItem(code);
    }

    @Override
    public ResponseDto updateItem(Item item) {
        return new ResponseDto(inventoryRepository.updateItem(item), null);
    }

    @Override
    public ResponseDto deleteItem(int code) {
        return new ResponseDto(inventoryRepository.deleteItem(code), null);

    }
}
