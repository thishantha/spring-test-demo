package com.training.springtestdemo.service;

import com.training.springtestdemo.model.Item;
import com.training.springtestdemo.model.dto.ResponseDto;

public interface InventoryService {
    ResponseDto addItem(Item item);

    ResponseDto getItem(int code);

    ResponseDto updateItem(Item item);

    ResponseDto deleteItem(int code);
}
