package com.training.springtestdemo.controller;

import com.training.springtestdemo.model.Item;
import com.training.springtestdemo.model.dto.ResponseDto;
import com.training.springtestdemo.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;

@RestController
@RequestMapping(value = "/api/inventory")
public class InventoryController {

    @Autowired
    InventoryService inventoryService;

    @GetMapping
    public ResponseDto getItem(@PathParam(value = "code") int code) {
        return inventoryService.getItem(code);
    }

    @PostMapping
    public ResponseDto addItem(@RequestBody Item item) {
        return inventoryService.addItem(item);
    }

    @PutMapping
    public ResponseDto updateItem(@RequestBody Item item) {
        return inventoryService.updateItem(item);
    }

    @DeleteMapping
    public ResponseDto deleteItem(@PathParam(value = "code") int code) {
        return inventoryService.deleteItem(code);
    }
}
